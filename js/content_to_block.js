(function ($) {
    Drupal.behaviors.content_to_block = {
        attach: function (context, settings) {
            $.each(settings.content_to_block, function(targetId, options) {
                var target = $('#' + targetId);
                var attachBehaviors = false;
                $(target, context).once('content-to-block-' + targetId, function () {
                    switch (options.source_type) {
                        case 'html_element':
                            // Get the source element.
                            var source = $(options.source_selector);
                            // Make sure that only a single element was matched.
                            if (source.length !== 1) {
                                return;
                            }
                            // Copy the source.
                            var source_cloned = source.clone();
                            // Remove the source.
                            source.remove();
                            // Remove html comments from beginning and end of
                            // the dom.
                            source_cloned.html(source_cloned.html().trim().replace(/^<!--/g, "").replace(/-->$/g, ""));
                            // Move the new source element to the target element.
                            target.html(source_cloned);
                            break;

                        case 'javascript_variable':
                            // Take the variable reference and get it's value
                            // which should be a string of HTML.
                            var source_text = eval(options.source_selector);
                            if (typeof source_text !== 'string') {
                                return;
                            }
                            // Place the text of the variable in the target.
                            target.html(source_text);
                            break;

                    }
                    // Flag to attach behaviors if any target needs it.
                    if (!attachBehaviors && options.attach_behaviors > 0) {
                        attachBehaviors = true;
                    }
                });
                // After we have gone through each target, attach behaviors only
                // once if needed.
                if (attachBehaviors) {
                    Drupal.attachBehaviors(document, Drupal.settings);
                }
            });
        }
    };

})(jQuery);
