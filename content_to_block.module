<?php
/**
 * @file Provides hooks and shared functions for Content To Block.
 */

/**
 * Implements hook_menu().
 */
function content_to_block_menu() {
  $menu_items = array();
  $menu_items['admin/structure/content_to_block'] = array(
    'title' => 'Content To Block',
    'description' => 'Create new content to block blocks that can be placed on pages. If you are using CTools, you can also use the supplied content type plugin without creating a block here.',
    'page callback' => 'content_to_block_list_blocks',
    'access arguments' => array('content_to_block list blocks'),
    'file' => 'content_to_block.admin.inc',
  );
  $menu_items['admin/structure/content_to_block/add'] = array(
    'title' => 'Create New Content To Block block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('content_to_block_block_form'),
    'access arguments' => array('content_to_block add blocks'),
    'file' => 'content_to_block.admin.inc',
  );
  $menu_items['admin/structure/content_to_block/%content_to_block/edit'] = array(
    'title' => 'Edit A Content To Block Block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('content_to_block_block_form', 3),
    'access arguments' => array('content_to_block edit blocks'),
    'file' => 'content_to_block.admin.inc',
  );
  $menu_items['admin/structure/content_to_block/%content_to_block/delete'] = array(
    'title' => 'Delete A Content To Block Block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('content_to_block_block_delete_form', 3),
    'access arguments' => array('content_to_block delete blocks'),
    'file' => 'content_to_block.admin.inc',
  );

  return $menu_items;
}

/**
 * Implements hook_permission().
 */
function content_to_block_permission() {
  return array(
    'content_to_block list blocks' => array(
      'title' => t('View all Content By Block blocks'),
      'description' => t('View all Content By Block blocks.'),
    ),
    'content_to_block add blocks' => array(
      'title' => t('Add Content By Block blocks'),
      'description' => t('Add Content By Block blocks.'),
    ),
    'content_to_block edit blocks' => array(
      'title' => t('Edit any Content By Block block'),
      'description' => t('Edit any Content By Block block.'),
    ),
    'content_to_block delete blocks' => array(
      'title' => t('Delete any Content By Block block'),
      'description' => t('Delete any Content By Block block.'),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function content_to_block_block_info() {
  $blocks = array();
  foreach (_content_to_block_get_blocks() as $block_delta => $block_conf) {
    $blocks[$block_delta] = array(
      'info' => 'Content To Block: ' . _content_to_block_admin_title($block_conf),
    );
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function content_to_block_block_view($delta = '') {
  $block = array();
  $renderable_block = content_to_block_renderable_block(content_to_block_load($delta));
  if (FALSE === $renderable_block) {
    return $block;
  }
  if (!empty($renderable_block['title'])) {
    $block['subject'] = $renderable_block['title'];
  }
  $block['content'] = render($renderable_block['content']);

  return $block;
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function content_to_block_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return "plugins/$plugin_type";
  }
}

/**
 * Implements hook_ctools_block_info().
 */
function content_to_block_ctools_block_info($module, $delta, &$info) {
  $info['category'] = t('Content To Block');
}

/**
 * Retrieve the admin title of a Content To Block block.
 *
 * @param array $block_conf
 *   Stored settings for the block.
 *
 * @return string
 *   The admin title.
 */
function _content_to_block_admin_title(array $block_conf) {
  if (!empty($block_conf['admin_title'])) {
    $admin_title = $block_conf['admin_title'];
  }
  else {
    $admin_title = $block_conf['source_selector'];
  }

  return substr($admin_title, 0, 60);
}

/**
 * Returns the $block_conf for a single Content To Block block.
 *
 * This is a %load function used by menu items.
 *
 * @param string $block_delta
 *   The block_delta is the 'target_id' key of $block_conf.
 *
 * @return array
 *   Block configuration for a single block if it's found.
 */
function content_to_block_load($block_delta) {
  $blocks = _content_to_block_get_blocks();
  if (isset($blocks[$block_delta])) {
    return $blocks[$block_delta];
  }

  return array();
}

/**
 * Remove a Content To Block block.
 *
 * @param string $block_delta
 *   The block_delta is the 'target_id' key of $block_conf.
 */
function _content_to_block_delete($block_delta) {
  $blocks = _content_to_block_get_blocks();
  if (isset($blocks[$block_delta])) {
    unset($blocks[$block_delta]);
    _content_to_block_save($blocks);
  }
}

/**
 * Save an array of Content To Block blocks.
 *
 * @param array $blocks
 *   This should be the results of a variable_get on the same variable modified.
 */
function _content_to_block_save(array $blocks) {
  variable_set('content_to_block_blocks', $blocks);
}

/**
 * Return an array of Content To Block blocks.
 *
 * @return array
 *   An array of blocks.
 */
function _content_to_block_get_blocks() {
  return variable_get('content_to_block_blocks', array());
}

/**
 * Returns an array of info needed to create a Content To Block block.
 *
 * @param array $block_conf
 *   An array of block configuration options.
 *
 * @return array
 *   An array of with keys of title and content which correspond to values
 *   needed to create a block.
 */
function content_to_block_renderable_block($block_conf) {
  $content = array(
    'markup' => array(
      '#type' => 'markup',
      '#markup' => '<div id="' . $block_conf['target_id'] . '"></div>',
      '#attached' => array(
        'js' => array(
          drupal_get_path('module', 'content_to_block') . '/js/content_to_block.js' => array(
            'type' => 'file',
          ),
          array(
            'data' => array(
              'content_to_block' => array(
                $block_conf['target_id'] => array(
                  'source_selector' => $block_conf['source_selector'],
                  'attach_behaviors' => $block_conf['attach_behaviors'],
                  'source_type' => $block_conf['source_type'],
                  'target_id' => $block_conf['target_id'],
                ),
              ),
            ),
            'type' => 'setting',
          ),
        ),
      ),
    ),
  );

  $title = '';
  if (!empty($block_conf['override_title']) && !empty($block_conf['override_title_text'])) {
    $title = $block_conf['override_title_text'];
  }
  elseif (!empty($block_conf['title'])) {
    $title = $block_conf['title'];
  }

  return array('content' => $content, 'title' => $title);
}

/**
 * Return the display value of a stored source_type.
 *
 * @param string $stored
 *   The stored value of source_type.
 *
 * @return string
 *   The display value.
 */
function _content_to_block_source_type_display($stored) {
  switch ($stored) {
    case 'html_element':
      return t('HTML Element');

    case 'javascript_variable':
      return t('Javascript Variable');

    default:
      return t('Invalid value');
  }
}

/**
 * Modify the given $form and add shared form elements.
 *
 * These are the shared form elements between the ctools content type and the
 * block configuration page.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 * @param array $block_conf
 *   An array of block configuration options.
 */
function content_to_block_block_form_shared(array &$form, array &$form_state, array $block_conf) {
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin Title'),
    '#description' => t('You will see this text when placing the block in the admin interfaces. If you enter nothing here, the Source Select field will be used.'),
    '#default_value' => !empty($block_conf['admin_title']) ? $block_conf['admin_title'] : '',
  );
  $form['source_type'] = array(
    '#type' => 'radios',
    '#title' => t('What type of source is this?'),
    '#description' => t('This module supports moving content from HTML elements and javascript variables.'),
    '#options' => array(
      'html_element' => _content_to_block_source_type_display('html_element'),
      'javascript_variable' => _content_to_block_source_type_display('javascript_variable'),
    ),
    '#default_value' => !empty($block_conf['source_type']) ? $block_conf['source_type'] : 'html_element',
  );
  $form['source_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Source Selector'),
    '#description' => t('<ul><li>For <strong>HTML Elements</strong>: The jQuery selector that will be used to find your source element that you want moved (Include the "#" for IDs and "." for classes, etc).</li><li>For <strong>Javascript Variable</strong>: The full name of the global javascript variable that holds the content (eg. Drupal.settings.my_module.great_text).</li></ul>'),
    '#default_value' => !empty($block_conf['source_selector']) ? $block_conf['source_selector'] : '',
    '#required' => TRUE,
  );
  $form['attach_behaviors'] = array(
    '#type' => 'checkbox',
    '#title' => t('Does the source require attaching behaviors to work properly?'),
    '#description' => t('If the source element is targeted by Drupal behaviors, Drupal.attachBehaviors() will be run after the element is moved.'),
    '#default_value' => !empty($block_conf['attach_behaviors']) ? $block_conf['attach_behaviors'] : '',
  );
  $form['target_id'] = array(
    '#type' => 'value',
    '#value' => !empty($block_conf['target_id']) ? $block_conf['target_id'] : substr('target_' . drupal_random_key(), 0, 32),
  );
}
