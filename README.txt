CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The Content To Block module provides the ability to place blocks on the page
that will take the content of another element and place it in itself. The
module currently supports moving HTML elements and Javascript global variables.
The module also supports two methods for creating blocks. You can create
custom blocks in the pages admin section that can be placed or if you have the
Ctools module, you can create content type plugin blocks. The admin section
blocks are stored in a single variable (content_to_block_blocks) while the
content type plugin blocks are stored in whatever Ctools panel you are on.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/mattsqd/2481123


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2481123

REQUIREMENTS
------------
This module has no required modules.

RECOMMENDED MODULES
-------------------
 * Chaos tool suite (https://www.drupal.org/project/ctools):
   When enabled, the content type 'Content to Block - CTools Content Type' will
   be available.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:


   - View all Content By Block blocks


     Required in order to view the blocks created at
     admin/structure/content_to_block.


   - Add Content By Block blocks


     Required to create new blocks at admin/structure/content_to_block/add.


   - Edit any Content By Block block


     Required to edit any block.

   - Delete any Content By Block block


     Required to delete any block.


TROUBLESHOOTING
---------------
 * I created a block that references an item on the page and my new block has
   no content:


   - Did you make sure use a valid jQuery selector that includes things like
     a "." for a class or "#" for an ID?

   - Did you use a jQuery selector that matched more than one element? You may
     only reference a single element.

   - Make sure that the selector or javascript variable are really on the same
     page as the block.

 * I created a block that references an item on the page and my new block
   does not look right:

   - The content may be targeted by a Drupal Behavior, edit your block and click
     the box for "Attach Drupal Behaviors".

   - Your selector maybe referencing content that contains Javascript that is
     being run inline or not through a Drupal Behavior. If this is the case you
     can fix this by editing the source text and placing HTML comments around
     the content so that the Javascript does not run. The included Javascript
     will strip these off and move your content.


MAINTAINERS
-----------
Current maintainers:
 * Matt Poole (mattsqd) - https://www.drupal.org/user/892310