<?php
/**
 * @file Content type plugin definition.
 */

$plugin = array(
  'title' => t('Content to Block - CTools Content Type'),
  'description' => t('Moves content on the page to a block.'),
  'single' => TRUE,
  'content_types' => array('content_to_block'),
  'render callback' => 'content_to_block_content_type_render',
  'edit form' => 'content_to_block_content_type_edit_form',
  'admin info' => 'content_to_block_content_type_admin_info',
  'category' => array(t('Content To Block'), -9),
);

/**
 * Ctools content type plugin edit form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 *
 * @return array $form
 *   The form after it's been modified.
 */
function content_to_block_content_type_edit_form(array $form, array &$form_state) {
  content_to_block_block_form_shared($form, $form_state, $form_state['conf']);

  return $form;
}

/**
 * Ctools edit form submit handler.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 */
function content_to_block_content_type_edit_form_submit(array $form, array &$form_state) {
  foreach (array(
             'source_selector',
              'attach_behaviors',
              'source_type',
              'target_id',
              'admin_title') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Render callback function.
 *
 * @param string $subtype
 *   The name of the subtype being rendered.
 * @param array $conf
 *   The configuration for the content type.
 * @param array $args
 *   The arguments provided to the owner of the content type.
 * @param array|null $context
 *   An array of context objects available for use.
 *
 * @return object
 *   The block to be rendered.
 */
function content_to_block_content_type_render($subtype, array $conf, array $args, $context) {
  $block = new stdClass();
  $renderable_block = content_to_block_renderable_block($conf);
  if (FALSE === $renderable_block) {
    return $block;
  }
  if (!empty($renderable_block['title'])) {
    $block->title = $renderable_block['title'];
  }
  $block->content = render($renderable_block['content']);

  return $block;
}

/**
 * Admin info callback function.
 *
 * @param string $subtype
 *   The name of the subtype being rendered.
 * @param array $conf
 *   The configuration for the content type.
 * @param array|null $contexts
 *   An array of context objects available for use.
 *
 * @return object
 *   The block info to be shown in admin.
 */
function content_to_block_content_type_admin_info($subtype, array $conf, $contexts) {
  $block = new stdClass();
  $block->title = _content_to_block_admin_title($conf);
  $block->content = '';

  return $block;
}
