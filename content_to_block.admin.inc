<?php
/**
 * @file Providers administration interface.
 */

/**
 * Provides a listing of all Content To Block blocks.
 *
 * @return string
 *   The listing of all blocks.
 */
function content_to_block_list_blocks() {
  $table = array(
    'header' => array(
      'Admin Display',
      'Source Selector',
      'Type',
      'Attach Behaviors',
      'Block Title',
      'Operations',
    ),
    'rows' => array(),
  );

  foreach (_content_to_block_get_blocks() as $block_delta => $block_conf) {
    // Determine if they have the permissions for the operations.
    $operations = array();
    if (user_access('content_to_block edit blocks') || user_access('content_to_block edit blocks')) {
      if (user_access('content_to_block edit blocks')) {
        $operations[] = l(t('Edit'), 'admin/structure/content_to_block/' . $block_conf['target_id'] . '/edit');
      }
      if (user_access('content_to_block edit blocks')) {
        $operations[] = l(t('Delete'), 'admin/structure/content_to_block/' . $block_conf['target_id'] . '/delete');
      }
    }
    $row = array(
      _content_to_block_admin_title($block_conf),
      $block_conf['source_selector'],
      _content_to_block_source_type_display($block_conf['source_type']),
      $block_conf['attach_behaviors'] ? 'Yes' : 'No',
      $block_conf['title'],
      implode(' | ', $operations),
    );
    $table['rows'][] = $row;
  }

  $return = '';
  if (user_access('content_to_block add blocks')) {
    $return .= "<p>" . l(t('Add New Block'), 'admin/structure/content_to_block/add') . "</p>";
  }
  $return .= theme('table', $table);

  return $return;
}

/**
 * Returns the form that is used to create or edit Content To Block block.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 * @param array $block_conf
 *   Stored settings for the block.
 *
 * @return array
 *   The modified form.
 */
function content_to_block_block_form(array $form, array &$form_state, $block_conf = array()) {
  content_to_block_block_form_shared($form, $form_state, $block_conf);
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Title'),
    '#description' => t('This is the front facing block title. It is not required to place a title on the block.'),
    '#default_value' => !empty($block_conf['title']) ? $block_conf['title'] : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => !empty($block_conf['target_id']) ? 'Edit Block' : 'Add Block',
  );

  return $form;
}

/**
 * Processes the form that is used to create or edit Content To Block block.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 */
function content_to_block_block_form_submit(array $form, array &$form_state) {
  $blocks = _content_to_block_get_blocks();
  $values = $form_state['values'];
  $blocks[$values['target_id']] = array(
    'target_id' => $values['target_id'],
    'source_type' => $values['source_type'],
    'source_selector' => $values['source_selector'],
    'attach_behaviors' => $values['attach_behaviors'],
    'admin_title' => $values['admin_title'],
    'title' => $values['title'],
  );
  // Update the variable with the new / updated block.
  _content_to_block_save($blocks);
  if ($values['op'] == 'Edit Block') {
    $message = t('Block has been updated.');
  }
  else {
    $message = t('Block has been added.');
  }

  $message .= ' ' . l(t('Click here to edit.'), 'admin/structure/content_to_block/' . $values['target_id'] . '/edit');
  drupal_set_message($message, 'sucess');
  $form_state['redirect'] = 'admin/structure/content_to_block';
}

/**
 * Returns the form that is used to delete Content To Block block.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 * @param array $block_conf
 *   Stored settings for the block.
 *
 * @return array
 *   The modified form.
 */
function content_to_block_block_delete_form(array $form, array &$form_state, $block_conf = array()) {
  if (empty($block_conf['target_id'])) {
    drupal_set_message('Cannot remove block, it does not exist.', 'error');
    drupal_goto('admin/structure/content_to_block');
  }
  drupal_set_message('Are you sure you want to delete this block?', 'warning');
  $form['target_id'] = array(
    '#type' => 'value',
    '#value' => $block_conf['target_id'],
  );
  $form['confirm'] = array(
    '#type' => 'submit',
    '#value' => 'Confirm',
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel',
  );

  return $form;
}

/**
 * Processes the form that is used to delete Content To Block block.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 */
function content_to_block_block_delete_form_submit(array $form, array &$form_state) {
  $values = $form_state['values'];
  switch ($values['op']) {
    case 'Confirm':
      _content_to_block_delete($values['target_id']);
      drupal_set_message('Block has been deleted.');
      break;

    case 'Cancel':
      drupal_set_message('Operation cancelled.');
      break;

  }
  $form_state['redirect'] = 'admin/structure/content_to_block';
}
